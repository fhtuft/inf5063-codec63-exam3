foreman_4k.yuv, 10 frames.

gprof:
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 48.61      0.70     0.70  1944000     0.00     0.00  write_block
 35.42      1.21     0.51  1749600     0.00     0.00  mc_block_8x8
  5.90      1.30     0.09 28145641     0.00     0.00  put_bits
  2.78      1.34     0.04 11252835     0.00     0.00  bit_width


nvprof:

==28675== Profiling result:
Time(%)      Time     Calls       Avg       Min       Max  Name
 80.52%  3.55531s        27  131.68ms  16.212ms  373.21ms  cuda_me_block_8x8
 10.66%  470.72ms        30  15.691ms  7.7197ms  34.691ms  cuda_dequant_idct_block_8x8
  8.69%  383.57ms        30  12.786ms  6.3069ms  28.470ms  cuda_dct_quant_block_8x8
  0.13%  5.5275ms        90  61.416us  8.0320us  223.20us  [CUDA memset]

==28675== API calls:
Time(%)      Time     Calls       Avg       Min       Max  Name
 77.37%  3.55658s        39  91.194ms  7.2420us  373.27ms  cudaMemcpy
 18.58%  854.26ms        20  42.713ms  37.820ms  52.088ms  cudaDeviceSynchronize
  2.49%  114.65ms       153  749.31us  8.0860us  103.19ms  cudaMallocManaged
  0.79%  36.130ms        90  401.44us  16.007us  3.2147ms  cudaMemset
  0.58%  26.546ms        87  305.12us  3.1540us  2.7475ms  cudaLaunch
  0.15%  7.0883ms       144  49.224us  6.4450us  126.71us  cudaFree


output:

Encoding frame 0, time ms: 256 Done!
Encoding frame 1, time ms: 724 Done!
Encoding frame 2, time ms: 713 Done!
Encoding frame 3, time ms: 715 Done!
Encoding frame 4, time ms: 716 Done!
Encoding frame 5, time ms: 717 Done!
Encoding frame 6, time ms: 718 Done!
Encoding frame 7, time ms: 716 Done!
Encoding frame 8, time ms: 717 Done!
Encoding frame 9, time ms: 717 Done!
(totally 6.71 seconds)


Kommentar:

Nå tar "cuda_me_block_8x8" bare 3.56 sekunder. Vi tror at en del av
grunnen til at det går såppass mye raskere også kan være at vi får
bedre occupancy per multiprosessor.

Denne versjonen finner tidvis andre motion vectorer enn den
opprinnelige versjonen, men etter en grundig undersøkelse viser at
disse har like god sad som den opprinnelige. Forskjellene vil
imidlertid akkumulere seg for senere frames; på grunn av andre motion
vectorer rekonstrueres andre bilder (på grunn av unøyaktigheter og
avrundinger andre steder) og gir derfor senere andre sad-er (og
best_sad-er). Dette skal ikke være noe problem, siden encodingen skal
være like god.
