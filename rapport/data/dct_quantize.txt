foreman_4k.yuv, 10 frames.

gprof:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 51.87      4.86     4.86 31104000     0.00     0.00  idct_1d
  9.39      5.74     0.88  1944000     0.00     0.00  dequantize_block
  8.32      6.52     0.78  1944000     0.00     0.00  dequant_idct_block_8x8
  7.90      7.26     0.74     5400     0.14     1.52  dequantize_idct_row
  5.76      7.80     0.54  3888000     0.00     0.00  transpose_block
  5.55      8.32     0.52  1749600     0.00     0.00  mc_block_8x8
  4.70      8.76     0.44  1944000     0.00     0.00  write_block
  4.16      9.15     0.39  1944000     0.00     0.00  scale_block
  1.17      9.26     0.11 28148780     0.00     0.00  put_bits

nvprof:

==9530== Profiling result:
Time(%)      Time     Calls       Avg       Min       Max  Name
 98.11%  20.2535s        27  750.13ms  21.131ms  2.35106s  cuda_me_block_8x8
  1.86%  383.54ms        30  12.785ms  6.3069ms  28.448ms  cuda_dct_quant_block_8x8
  0.03%  5.5295ms        90  61.438us  8.0000us  222.91us  [CUDA memset]

==9530== API calls:
Time(%)      Time     Calls       Avg       Min       Max  Name
 96.93%  20.2549s        39  519.36ms  6.7540us  2.35111s  cudaMemcpy
  1.84%  383.53ms        10  38.353ms  37.820ms  42.686ms  cudaDeviceSynchronize
  0.77%  161.87ms       153  1.0580ms  8.7150us  150.25ms  cudaMallocManaged
  0.29%  59.696ms        90  663.29us  15.780us  5.8296ms  cudaMemset
  0.13%  27.001ms        57  473.70us  3.4480us  2.7949ms  cudaLaunch
  0.03%  7.2639ms       144  50.443us  6.1510us  165.31us  cudaFree


output:

Encoding frame 0, time ms: 1057 Done!
Encoding frame 1, time ms: 3365 Done!
Encoding frame 2, time ms: 3363 Done!
Encoding frame 3, time ms: 3366 Done!
Encoding frame 4, time ms: 3367 Done!
Encoding frame 5, time ms: 3373 Done!
Encoding frame 6, time ms: 3368 Done!
Encoding frame 7, time ms: 3367 Done!
Encoding frame 8, time ms: 3369 Done!
Encoding frame 9, time ms: 3394 Done!
(totally ~31 seconds)


Kommentar:
Vi ser av gprof at cpu-en går fra å bruke ~16.5 sekunder til sammen
til å bruke ~9.4 sekunder til sammen på funksjoner som faktisk gjør
noe med filmen.

Det er verdt å merke at "cudaMemcpy" bruker like mye tid
som "cuda_me_block_8x8". Dette er fordi at etter at cpu-en har startet
kernelen til me_block så kaller den cudaMemcpy og må vente til kernelen
er ferdig. De andre kallene på cudaMemcpy bruker nesten ikke noe
tid. Tilsvarende bruker "cudaDeviceSynchronize" like lang tid som
"cuda_dct_quant_block".
