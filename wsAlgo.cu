/*
Work schedule algorithme
*/
#define WSADEBUG 0

#include <sys/time.h> //for gettime
#include <stdio.h>
#include "wsAlgo.h"

#define EPSILON 10  //ms

static unsigned long long startTime =0;  /*Messures the time master hangs, or not hangs (no time is messured for this, which can be a problem), on entry to read_yuv_master. This is used as a signal to increase the work of master or slave*/ 

static unsigned long long diffTime; //if larger then EPSILON work reduced for Slave




/*Return the time in milliseconds since the epoch
 returns: unsigned long long, time in milliseconds
 */
unsigned long long gettime() {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (unsigned long long)(tv.tv_sec) * 1000 + (unsigned long long)(tv.tv_usec) / 1000;
}

/* Return the time elapsed since last time. Updates prev_time */
unsigned long long get_dt(unsigned long long *prev_time){
    unsigned long long curr_time = gettime();
    unsigned long long res = curr_time - *prev_time;
    *prev_time = curr_time;
    return res;
}


void wsStartTime() {
  startTime = gettime();
}

void wsDiffTime() {
  diffTime = get_dt(&startTime);
}

int wsScheduleAlgo() {
#ifdef WSADEBUG
  fprintf(stderr,"WSA: diffTime %llu\n",diffTime);
#endif

  if(diffTime > EPSILON)
    return 1; //reduce work for slave, increase work for master
  else
    return 0; //increase work for slave, reduce work for master
}
