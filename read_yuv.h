#ifndef READYUV
#define READYUV
/*
Versions of read_yuv for Singel, Master and Slave node.
*/

void init_read_yuv(uint32_t w, uint32_t h);
bool read_yuv(FILE *file, struct c63_common *cm);
bool read_yuv_slave(struct c63_common *cm,int frameNo,serverParams *slaveServ, int *topRow, int *nRows);
bool read_yuv_master(FILE *file,struct c63_common *cm,int frameNo,clientParams *masterCli);
void send_yuv(struct c63_common *cm, yuv_t * image,int frameNo,clientParams *masterCli, int topRow, int nRows);

#endif
