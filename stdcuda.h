#ifndef STDCUDA
#define STDCUDA

#include <stdio.h>

#define MAXTHREADS 1024
  
//must include <stdio.h>
static void cuda_error_msg(cudaError err, const char *msg) {
  if(err != cudaSuccess) {
    printf("CUDA_ERROR: msg:%s  %s",msg,cudaGetErrorString(err));
  }

}


static void cuda_error(cudaError err, const char *errFile,int lineNr) {
  if(err != cudaSuccess) {
    printf("CUDA_ERROR in %s at %d:   %s",errFile,lineNr,cudaGetErrorString(err));
  }

}

#define CUDA_ERROR_MSG(err,msg) cuda_error_msg(err,msg)
#define CUDA_ERROR(err)  cuda_error(err,__FILE__,__LINE__)



#endif
