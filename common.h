#ifndef C63_COMMON_H_
#define C63_COMMON_H_

#include <inttypes.h>

#include "c63.h"

enum Buftype{
  orig,
  recons,
  predicted,
  residuals
};

// Declaration

struct frame* create_frame(struct c63_common *cm, yuv_t *image, bool gpu = false);

void dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, int16_t *out_data, uint8_t *quantization);

void cuda_dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, int16_t *out_data, uint8_t *quantization);

void dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, uint8_t *out_data, uint8_t *quantization);

void cuda_dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, uint8_t *out_data, uint8_t *quantization);

void partial_dct_quantize(struct c63_common *cm, int color_component,
    int topRow, int nRows, bool gpu);

void partial_dequantize_idct(struct c63_common *cm, int color_component,
    int topRow, int nRows, bool gpu);

void free_yuv(yuv_t *image, int gpu = false);

inline uint8_t* select_yuv(yuv_t *yuv, int color_component){
  uint8_t* bufs[] = { yuv->Y, yuv->U, yuv->V };
  return bufs[color_component];
}
inline int16_t* select_dct(dct_t *dct, int color_component){
  int16_t *bufs[] = { dct->Ydct, dct->Udct, dct->Vdct };
  return bufs[color_component];
}

void* alloc_buf(size_t size, bool gpu);

/*
void copy_yuv(struct c63_common *cm, struct yuv *to, struct yuv *from,
    enum cudaMemcpyKind kind);
*/
void copy_yuv_cc_rows(struct c63_common *cm, struct yuv *to, struct yuv *from,
    int color_component, int topRow, int nRows, enum cudaMemcpyKind kind = cudaMemcpyHostToHost);

void swap_yuv(yuv_t **a, yuv_t **b);

void* getBuf(struct frame *f,
    Buftype buftype, int color_component);
size_t getBufSize(struct c63_common *cm, Buftype buftype,
    int color_component);
size_t getBufRowSize(struct c63_common *cm, Buftype buftype,
    int color_component);

size_t getMvSize(struct c63_common *cm, int color_component);

inline size_t getBufRowElemnts(struct c63_common *cm, int color_component){
  return cm->padw[color_component]*8;
}

void copyRows(struct c63_common *cm,
    struct frame *to_f, struct frame *from_f, Buftype buftype,
    int color_component, int topRow, int nRows, enum cudaMemcpyKind kind = cudaMemcpyHostToHost);
void copyBuf(struct c63_common *cm,
    struct frame *to_f, struct frame *from_f, Buftype buftype,
    int color_component, enum cudaMemcpyKind kind = cudaMemcpyHostToHost);


void destroy_frame(struct frame *f, bool gpu = false);

void dump_image(yuv_t *image, int w, int h, FILE *fp);

#endif  /* C63_COMMON_H_ */
