#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dsp.h"
#include "me.h"
#include "common.h"

#include <cuda.h> //for cuda magic!

#include "stdcuda.h"

/* Motion estimation for 8x8 block */
static void me_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb = &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;

  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {

      int sad;
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);

      if (sad < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = sad;
      }
    }
  }

#define PRINT_SAD 0
#if PRINT_SAD
      printf("sad: (%d,%d) %d\n", mb_x, mb_y, best_sad);
#endif
#undef PRINT_SAD


  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}

/* Motion estimation for 8x8 block */
static void cpu_me_block_8x8_o(int w, int h, int range, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, struct macroblock *mb)
{
  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;

  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {

      int sad;
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);

      if (sad < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = sad;
      }
    }
  }

#define PRINT_SAD 0
#if PRINT_SAD
      printf("sad: (%d,%d) %d\n", mb_x, mb_y, best_sad);
#endif
#undef PRINT_SAD


  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}

__global__ void cuda_me_block_8x8(uint8_t *orig, uint8_t *ref, int w, int h,
        int range, int8_t *mv_x, int8_t *mv_y)
{
    int mb_x = blockIdx.x;
    int mb_y = blockIdx.y;
    int x = threadIdx.x;
    int y = threadIdx.y;
    int blockId = blockIdx.y*gridDim.x + blockIdx.x;
    //int tid = threadIdx.y*blockDim.x + threadIdx.x;

    int left = mb_x * 8 - range;
    int top = mb_y * 8 - range;
    int right = mb_x * 8 + range;
    int bottom = mb_y * 8 + range;

    /* Make sure we are within bounds of reference frame. TODO: Support partial
        frame bounds. */
    if (left < 0) { left = 0; }
    if (top < 0) { top = 0; }
    if (right > (w - 8)) { right = w - 8; }
    if (bottom > (h - 8)) { bottom = h - 8; }

    int mx = mb_x * 8;
    int my = mb_y * 8;

    int sw = right-left; // search width
    int sh = bottom-top; // search height

    __shared__ int sad_buf[32][32];
    __shared__ int x_buf[32][32];
    __shared__ int y_buf[32][32];
    sad_buf[y][x] = INT_MAX;
    x_buf[y][x] = left + x - mx;
    y_buf[y][x] = top + y - my;

    //dev_sad_block_8x8(orig + my*w+mx, ref + top*w+left, sw, sh, w, sad_buf);
    //uint8_t *block1 = orig + my*w+mx;
    __shared__ uint8_t block1[8][8];
    if (x < 8 && y < 8){
      block1[y][x] = orig[(my + y)*w + (mx + x)];
    }
    __syncthreads();

    uint8_t *window = ref + top*w+left;


    int accum = 0;
    uint8_t *startPos = window + y*w+ x;
    int dx,dy;
    if (x < sw && y < sh){
#pragma unroll
      for (dy=0; dy<8; dy++){
#pragma unroll
	  for (dx=0; dx<8; dx++){
                accum += abs(block1[dy][dx] - startPos[dy*w+dx]);
            }
        }
        sad_buf[y][x] = accum;
    }
    __syncthreads();


#define USE_MIN_REDUCE 0
#if USE_MIN_REDUCE
    for (dx=blockDim.x/2; dx>0; dx/=2){
      if (x < dx){
        if (sad_buf[y][x + dx] < sad_buf[y][x]){
          sad_buf[y][x]  = sad_buf[y][x + dx];
          x_buf[y][x] = x_buf[y][x + dx];
          y_buf[y][x] = y_buf[y][x + dx];
        }
      }
      __syncthreads();
    }
    for (dy=blockDim.y/2; dy>0; dy/=2){
      if (x == 0 && y < dy){
        if (sad_buf[y + dy][0] < sad_buf[y][0]){
          sad_buf[y][0] = sad_buf[y + dy][0];
          x_buf[y][0] = x_buf[y + dy][0];
          y_buf[y][0] = y_buf[y + dy][0];
        }
      }
      __syncthreads();
    }

    if (x == 0 && y == 0){
#define PRINT_SAD 0
#if PRINT_SAD
      printf("(%d,%d) sad: %d => mv: (%d,%d)\n", mb_x, mb_y, sad_buf[0][0],
          x_buf[0][0],y_buf[0][0]);
#endif
#undef PRINT_SAD
      mv_x[blockId] = x_buf[0][0];
      mv_y[blockId] = y_buf[0][0];
    }
    return;
#else /* USE_MIN_REDUCE */

    //int x, y;
    //printf("(left, top) ~ (%d,%d) ~ (%d,%d)\n", left, top, mx,my);
    int best_sad = INT_MAX;
    if (x == 0 && y == 0){
        for (y = top; y < bottom; ++y)
        {
            for (x = left; x < right; ++x)
            {
                int sad = sad_buf[(y-top)][(x-left)];
#define ME_TEST 0
#if ME_TEST
                int sad2;
                sad_block_8x8(orig + my*8*w+mx, ref + y*w+x, w, &sad2);

                if (sad != sad2){
                    printf("(%d, %d), %d ~ %d\n", x, y, sad, sad2);
                }
#endif
#undef ME_TEST
                if (sad < best_sad)
                {
                    dx = x - mx;
                    dy = y - my;
                    best_sad = sad;
                }
            }
        }
#define PRINT_SAD 0
#if PRINT_SAD
        printf("(%d,%d) sad: %d => mv: (%d,%d)\n", mb_x, mb_y, best_sad,dx,dy);
#endif
#undef PRINT_SAD
        mv_x[blockId] = dx;
        mv_y[blockId] = dy;
    }
#endif /* USE_MIN_REDUCE */
}
/*
4k:
cm->mb_rows = 270 macro blocks
cm->mb_cols = 480 macro blocks
total macro blocks = 129600
foreman:
cm->mb_rows = 36 macro blocks
cm->mb_cols = 44 macro blocks
total macro blocks = 1584
 
*/

void gpu_motion_estimate(uint8_t *orig, uint8_t *ref,
    int nCols, int nRows, int range,
    int8_t *mv_x, int8_t *mv_y, struct mv_buf_cut *mv_bufs)
{
  int nBlocks = nCols*nRows;

  // Prepare working buffers
  int8_t *dev_mv_x, *dev_mv_y;
  CUDA_ERROR( cudaMalloc(&dev_mv_x, nBlocks*sizeof(int8_t)) );
  CUDA_ERROR( cudaMalloc(&dev_mv_y, nBlocks*sizeof(int8_t)) );



  // preparing threads and blocks
  dim3 blocks = dim3(nCols, nRows);
  dim3 threads = dim3(2*range, 2*range);

  // do the search
  cuda_me_block_8x8<<<blocks,threads>>>(orig, ref,
          nCols*8, nRows*8, range, dev_mv_x, dev_mv_y);


  /*
  //Do this later instead
  CUDA_ERROR( cudaMemcpy(mv_x, dev_mv_x, nBlocks*sizeof(int8_t),
              cudaMemcpyDeviceToHost) );
  CUDA_ERROR( cudaMemcpy(mv_y, dev_mv_y, nBlocks*sizeof(int8_t),
              cudaMemcpyDeviceToHost) );

  CUDA_ERROR( cudaFree(dev_mv_x) );
  CUDA_ERROR( cudaFree(dev_mv_y) );
  */

  mv_bufs->dev_mv_x = dev_mv_x;
  mv_bufs->dev_mv_y = dev_mv_y;
}

void cpu_motion_estimate(uint8_t *orig, uint8_t *ref,
    int nCols, int nRows, int range,
    int8_t *mv_x, int8_t *mv_y)
{
  // TODO: implement cpu_motion_estimate
  int mb_x, mb_y;
  struct macroblock mb;
  for (mb_y = 0; mb_y < nRows; ++mb_y)
  {
    for (mb_x = 0; mb_x < nCols; ++mb_x)
    {
      cpu_me_block_8x8(8*nCols, 8*nRows, range, mb_x, mb_y, orig,
          ref, &mb);
      mv_x[mb_y*nCols + mb_x] = mb.mv_x;
      mv_y[mb_y*nCols + mb_x] = mb.mv_y;
    }
  }
}

void c63_start_partial_motion_estimate(struct c63_common *cm, int color_component,
      int topRow, int nRows, bool gpu, struct mv_buf_cut *mv_bufs){
  // preparing variables
  int totalRows = cm->mb_rows;
  int nCols = cm->mb_cols;
  int range = cm->me_search_range;
  if (color_component != Y_COMPONENT){
    totalRows /= 2;
    nCols /= 2;
    range /= 2;
  }

  int endRow = topRow + nRows; // first row after the rows to calculate

  if (topRow < 0){
    fprintf(stderr, "error: topRow = %d < 0\n", topRow);
    exit(EXIT_FAILURE);
  }
  if (endRow > totalRows){
    fprintf(stderr, "error: endRow = %d > %d\n", topRow+nRows, totalRows);
    exit(EXIT_FAILURE);
  }

  int extraRowsAbove = range/8;
  int extraRowsBelow = range/8;
  if ((topRow-extraRowsAbove) < 0){
    extraRowsAbove = topRow;
    //printf("Reduced extraRowsAbove... (topRow=%d)\n", topRow);
  }
  if ((endRow+extraRowsBelow) > totalRows){
    extraRowsBelow = totalRows - endRow;
    //printf("Reduced extraRowsBelow... (endRow=%d, extraRowsBelow=%d)\n", endRow, extraRowsBelow);
  }
  totalRows = extraRowsAbove + nRows + extraRowsBelow; // redefinition
  int nBlocks = totalRows*nCols;

  // preparing result buffers
  int8_t *mv_x, *mv_y;
  mv_x = (int8_t*) malloc(nBlocks*sizeof(int8_t));
  mv_y = (int8_t*) malloc(nBlocks*sizeof(int8_t));

  int topRowIdx = (topRow-extraRowsAbove) * getBufRowSize(cm, orig, color_component);

  // select correct frame
  struct frame *curframe = cm->curframe;
  struct frame *refframe = cm->refframe;
  if (gpu){
    curframe = cm->dev_curframe;
    refframe = cm->dev_refframe;
  }

  // select the correct buffer
  uint8_t *orig_buf = select_yuv(curframe->orig, color_component);
  uint8_t *ref_buf  = select_yuv(refframe->recons, color_component);

  if (gpu){
    gpu_motion_estimate(&orig_buf[topRowIdx], &ref_buf[topRowIdx],
        nCols, totalRows, range, mv_x, mv_y, mv_bufs);
  } else {
    cpu_motion_estimate(&orig_buf[topRowIdx], &ref_buf[topRowIdx],
        nCols, totalRows, range, mv_x, mv_y);
  }

  mv_bufs->mv_x = mv_x;
  mv_bufs->mv_y = mv_y;
  mv_bufs->extraRowsAbove = extraRowsAbove;
  mv_bufs->extraRowsBelow = extraRowsBelow;
  mv_bufs->topRow = topRow;
  mv_bufs->nRows = nRows;
  mv_bufs->nCols = nCols;
  mv_bufs->color_component = color_component;
  mv_bufs->gpu = gpu;
}

void c63_finalize_partial_motion_estimate(struct c63_common *cm, struct mv_buf_cut *mv_bufs){

  int8_t *mv_x  = mv_bufs->mv_x;
  int8_t *mv_y  = mv_bufs->mv_y;
  int8_t *dev_mv_x = mv_bufs->dev_mv_x;
  int8_t *dev_mv_y = mv_bufs->dev_mv_y;
  int extraRowsAbove = mv_bufs->extraRowsAbove;
  int extraRowsBelow = mv_bufs->extraRowsBelow;
  int topRow = mv_bufs->topRow;
  int nRows = mv_bufs->nRows;
  int nCols = mv_bufs->nCols;
  int color_component = mv_bufs->color_component;
  bool gpu = mv_bufs->gpu;
  
  int totalRows = extraRowsAbove + nRows + extraRowsBelow;
  int nBlocks = totalRows*nCols;

  if (gpu){
    CUDA_ERROR( cudaMemcpy(mv_x, dev_mv_x, nBlocks*sizeof(int8_t),
                cudaMemcpyDeviceToHost) );
    CUDA_ERROR( cudaMemcpy(mv_y, dev_mv_y, nBlocks*sizeof(int8_t),
                cudaMemcpyDeviceToHost) );

    CUDA_ERROR( cudaFree(dev_mv_x) );
    CUDA_ERROR( cudaFree(dev_mv_y) );
  }
  
  mv_x += extraRowsAbove*nCols;
  mv_y += extraRowsAbove*nCols;

  
  int mb_x, mb_y;
#define ME_TEST 0
#if ME_TEST
  if (gpu){
    copyBuf(cm, cm->curframe, cm->dev_curframe, orig, color_component,
        cudaMemcpyDeviceToHost);
    copyBuf(cm, cm->refframe, cm->dev_refframe, recons , color_component,
        cudaMemcpyDeviceToHost);
  }
  for (mb_y = 0; mb_y < nRows; ++mb_y)
  {
    for (mb_x = 0; mb_x < nCols; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y+topRow,
          select_yuv(cm->curframe->orig, color_component),
          select_yuv(cm->refframe->recons, color_component), color_component);

      struct macroblock *mb =
            &cm->curframe->mbs[color_component][(mb_y+topRow)*nCols+mb_x];

      int8_t tmp_x = mv_x[mb_y*nCols+mb_x];
      int8_t tmp_y = mv_y[mb_y*nCols+mb_x];
      if (tmp_x != mb->mv_x || tmp_y != mb->mv_y){
        printf("Color %d: (%d, %d), (%d, %d) ~ (%d, %d)\n",
                color_component,
                mb_x, mb_y,
                mv_x[mb_y*nCols+mb_x], mv_y[mb_y*nCols+mb_x],
                mb->mv_x, mb->mv_y);
      }
    }
  }
#endif
#undef ME_TEST


  for (mb_y = 0; mb_y < nRows; ++mb_y)
  {
    for (mb_x = 0; mb_x < nCols; ++mb_x)
    {
      struct macroblock *mb =
            &cm->curframe->mbs[color_component][(mb_y+topRow)*nCols+mb_x];

      int8_t tmp_x = mv_x[mb_y*nCols+mb_x];
      int8_t tmp_y = mv_y[mb_y*nCols+mb_x];
      mb->mv_x = tmp_x;
      mb->mv_y = tmp_y;
      mb->use_mv = 1;
    }
  }
  free(mv_bufs->mv_x);
  free(mv_bufs->mv_y);
}

/* Motion compensation for 8x8 block */
 __global__ void cuda_mc_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *predicted, uint8_t *ref, int color_component)
{
  /*
    curframe: struct frame in cm
    mbs: struct macrblock *mbs in struct frame
    color_component: 0,1,2
    padw: int[color_components:3]
*/
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = cm->padw[color_component];

  // Copy block from ref mandated by MV 
  int x, y;
  
  //printf("bottom: %d  right: %d\n",bottom-top,right-left);
  
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
    }
  }
}

// Motion compensation for 8x8 block 
static void mc_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *predicted, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = cm->padw[color_component];
  //printf("w: %d\n",w);
  
  /* Copy block from ref mandated by MV */
  int x, y;
  
  //printf("bottom: %d  right: %d\n",bottom-top,right-left);
  
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
    }
  }
}

/* Motion compensation for 8x8 block */
/*
Tested: Yes
Bugs: Yes, ??
*/
__global__ void cuda_mc_block_8x8(struct c63_common *cm, uint8_t *predicted, uint8_t *ref, int w,int *use_mv,int *mv_x,int *mv_y)
 {
    int mb_x = blockIdx.x;
    int mb_y = blockIdx.y;
   
  
    //struct macroblock *mb =
    //&cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!use_mv[mb_x + mb_y*blockDim.x]) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
 
 

  //int w = cm->padw[color_component];

  
  /*
    (left,top) er kordinatene til høyre hjørne i denne spesiefike mb.
    x og y representerer de globale x og y kordinatene i bilde.
   */
  int x = threadIdx.x + left; 
  int y = threadIdx.y + top;
  /*
    mb_x + mb_y*blockDim.x: Gir posisjonen til data overført til en array av 
    dobble forløkker i c63_motion_compenstate.
*/
  predicted[y*w+x] = ref[(y + mv_y[mb_x + mb_y*blockDim.x] )*w  + (x + mv_x[mb_x + mb_y*blockDim.x])];

  //predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
  
}

void c63_motion_compensate(struct c63_common *cm)
{
  int mb_x, mb_y;

  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->Y,
          cm->refframe->recons->Y, Y_COMPONENT);
    }
  }

  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->U,
          cm->refframe->recons->U, U_COMPONENT);
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->V,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
}

// must have that the refframe->recons is on the gpu/cpu depending on where it is used
void c63_partial_motion_compensate(struct c63_common *cm, int color_component,
      int topRow, int nRows, bool gpu)
{
  // preparing variables
  int nCols = cm->mb_cols;
  if (color_component != Y_COMPONENT){
    nCols /= 2;
  }

  // select correct frame
  struct frame *curframe = cm->curframe;
  struct frame *refframe = cm->refframe;
  if (gpu){
    curframe = cm->dev_curframe;
    refframe = cm->dev_refframe;
  }

  if (gpu){
    //TODO: Support gpu_motion_compensate
  } else {
    int mb_x, mb_y;
    for (mb_y = 0; mb_y < nRows; ++mb_y){
      for (mb_x = 0; mb_x < nCols; ++mb_x){
        mc_block_8x8(cm, mb_x, topRow+mb_y,
            select_yuv(curframe->predicted, color_component),
            select_yuv(refframe->recons, color_component), color_component);
      }
    }
  }
}

/*
Tested: Yes
Bugs: Yes, give wrong output, probably from cuda_mc_block
*/
void c63_motion_compensate_cuda(struct c63_common *cm)
{
  int mb_x, mb_y;

  
  int color_component = Y_COMPONENT;
  struct macroblock *mb;
  int *mv_x,*mv_y; 
  int *use_mv;

  int *use_mv_cuda;
  int *mv_x_cuda,*mv_y_cuda;

  

  CUDA_ERROR(cudaMalloc(&use_mv_cuda,cm->mb_rows*cm->mb_cols*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_x_cuda,cm->mb_rows*cm->mb_cols*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_y_cuda,cm->mb_rows*cm->mb_cols*sizeof(int)));

  use_mv = (int*)malloc(cm->mb_rows*cm->mb_cols*sizeof(int));
  mv_x = (int*)malloc(cm->mb_rows*cm->mb_cols*sizeof(int));
  mv_y = (int*)malloc(cm->mb_rows*cm->mb_cols*sizeof(int));

  /* Luma */
  color_component = Y_COMPONENT; 
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      mb = &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];
      
      use_mv[mb_y*cm->mb_cols+mb_x] = mb->use_mv; 
      mv_x[mb_y*cm->mb_cols+mb_x] = mb->mv_x; 
      mv_y[mb_y*cm->mb_cols+mb_x] = mb->mv_y;
    }
  }
  
  CUDA_ERROR( cudaMemcpy((void *)use_mv_cuda,(void *)use_mv,
			 cm->mb_rows*cm->mb_cols*sizeof(int),cudaMemcpyHostToDevice));
  CUDA_ERROR( cudaMemcpy((void *)mv_x_cuda,(void *)mv_x,
			 cm->mb_rows*cm->mb_cols*sizeof(int),cudaMemcpyHostToDevice));
  CUDA_ERROR( cudaMemcpy((void *)mv_y_cuda,(void *)mv_y,
			 cm->mb_rows*cm->mb_cols*sizeof(int),cudaMemcpyHostToDevice));
  
  

  dim3 blocks = dim3(cm->mb_cols, cm->mb_rows);
  dim3 threads = dim3(8,8);
 
 
  cuda_mc_block_8x8<<<blocks,threads>>>(cm,cm->curframe->predicted->Y,cm->refframe->recons->Y,
					cm->padw[Y_COMPONENT],use_mv_cuda,mv_x_cuda,mv_y_cuda);
  CUDA_ERROR( cudaDeviceSynchronize() );
#define MC_TEST 0
#if MC_TEST
  uint8_t *test_outdata;
  int test_x,test_y;
  test_outdata = (uint8_t *)malloc(cm->mb_cols*cm->mb_rows*sizeof(uint8_t));
  for (mb_y = 0; mb_y < cm->mb_rows ; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols ; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, test_outdata,cm->refframe->recons->Y, Y_COMPONENT);
    }
  }
  
   for (mb_y = 0; mb_y < cm->mb_rows*8 ; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols*8 ; ++mb_x)
    {
      
      if(test_outdata[mb_x+mb_y*cm->mb_rows*8] != cm->curframe->predicted->Y[mb_x+mb_y*cm->mb_rows*8]) {
	printf("Y(%d,%d)\n",test_outdata[mb_x+mb_y*cm->mb_rows*8],
	       cm->curframe->predicted->Y[mb_x+mb_y*cm->mb_rows*8]);
      }

    }
  } 

  
#endif


  CUDA_ERROR(cudaFree(use_mv_cuda)); 
  CUDA_ERROR(cudaFree(mv_x_cuda)); CUDA_ERROR(cudaFree(mv_y_cuda));

  CUDA_ERROR(cudaMalloc(&use_mv_cuda,(cm->mb_rows/2)*(cm->mb_cols/2)*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_x_cuda,(cm->mb_rows/2)*(cm->mb_cols/2)*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_y_cuda,(cm->mb_rows/2)*(cm->mb_cols/2)*sizeof(int)));


  int cols = cm->mb_cols/2; 
  int rows = cm->mb_rows/2;
  
  /* Chroma U*/
  color_component = U_COMPONENT;
  for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    {
       mb = &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];
       use_mv[mb_y*cols+mb_x] = mb->use_mv; 
       mv_x[mb_y*cols+mb_x] = mb->mv_x; 
       mv_y[mb_y*cols+mb_x] = mb->mv_y;
    }
  }

  CUDA_ERROR( cudaMemcpy((void *)use_mv_cuda,(void *)use_mv,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_x_cuda,(void *)mv_x,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_y_cuda,(void *)mv_y,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
 

  blocks = dim3(cols, rows);
  threads = dim3(8,8);
  cuda_mc_block_8x8<<<blocks,threads>>>(cm,cm->curframe->predicted->U,cm->refframe->recons->U,
					cm->padw[U_COMPONENT],use_mv_cuda,mv_x_cuda,mv_y_cuda);
  CUDA_ERROR( cudaDeviceSynchronize() );
#if MC_TEST
 for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, test_outdata,
          cm->refframe->recons->U, U_COMPONENT);  
    }
  }
 
  for (mb_y = 0; rows*8 ; ++mb_y)
  {
    for (mb_x = 0; cols*8 ; ++mb_x)
    {
      if(test_outdata[mb_x+mb_y*rows*8] != cm->curframe->predicted->U[mb_x+mb_y*rows*8]) {
	printf("U(%d,%d)\n",test_outdata[mb_x+mb_y*rows*8],
	       cm->curframe->predicted->U[mb_x+mb_y*rows*8]);
      }
    }
  }
 
#endif


  /* Chroma V*/
  color_component = V_COMPONENT;
  for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    {
       mb = &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];
      
       use_mv[mb_y*cols+mb_x] = mb->use_mv; 
       mv_x[mb_y*cols+mb_x] = mb->mv_x; 
       mv_y[mb_y*cols+mb_x] = mb->mv_y;
    }
  }


  CUDA_ERROR( cudaMemcpy((void *)use_mv_cuda,(void *)use_mv,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_x_cuda,(void *)mv_x,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_y_cuda,(void *)mv_y,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );


  cuda_mc_block_8x8<<<blocks,threads>>>(cm,cm->curframe->predicted->V,cm->refframe->recons->V,
					cm->padw[V_COMPONENT],use_mv_cuda,mv_x_cuda,mv_y_cuda);
  CUDA_ERROR( cudaDeviceSynchronize() );
#if MC_TEST
 for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    { 
      mc_block_8x8(cm, mb_x, mb_y, test_outdata,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
 printf("TEST2\n");
  for (mb_y = 0; rows*8 ; ++mb_y)
  {
    for (mb_x = 0; cols*8 ; ++mb_x)
    {
      if(test_outdata[mb_x+mb_y*rows*8] != cm->curframe->predicted->V[mb_x+mb_y*rows*8]) {
	printf("V(%d,%d)\n",test_outdata[mb_x+mb_y*rows*8],
	       cm->curframe->predicted->V[mb_x+mb_y*rows*8]);
      }
    }
  }


  free(test_outdata);
#endif
#undef MC_TEST

  free(use_mv);
  free(mv_x);
  free(mv_y);
  
  CUDA_ERROR(cudaFree(use_mv_cuda)); 
  CUDA_ERROR(cudaFree(mv_x_cuda)); CUDA_ERROR(cudaFree(mv_y_cuda));
}

void c63_motion_estimate(struct c63_common *cm)
{
  /* Compare this frame with previous reconstructed frame */
  int mb_x, mb_y;

  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->Y,
          cm->refframe->recons->Y, Y_COMPONENT);
    }
  }

  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->U,
          cm->refframe->recons->U, U_COMPONENT);
    }
  }
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
    {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->V,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
}
