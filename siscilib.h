
#ifndef SISCILIB
#define SISCILIB

/*Prototypes for all public functions in siscilib
Usages:
unsigned int localNode;//dont forget this
server/clinetParams server/client;
server1.interruptNo = WRITEDONE; 
server1.offset =0;
server1.localSegmentId = 666; 
server1.remoteNodeId = 4;
server1.segmentSize = 8192; 
SISCI_ERROR(sisciInit(0,&localNodeId));
server.localNodeId  = localNodeId;
SISCI_ERROR(makeServerSegment(&server));
SISCI_ERROR(reciveData(&recive_buffer[0],sizeofrecivebuf,&server)); 
SISCI_ERROR(removeServer(&server));
*/
#include "/opt/DIS/include/sisci_api.h"
#include "/opt/DIS/include/sisci_error.h"

#define NO_CALLBACK        NULL
#define NO_FLAGS           0
#define NO_CALLBACK_P      NULL
/*Interrups*/
#define WRITEDONE          66
#define DEFAULTSEGMENTSIZE 60000000
                                                    

//Spesific nodeIdS for c63enc
#define MASTERSERVERNODEID  77
#define SLAVESERVERNODEID   83


#define SISCI_ERROR(err)  sisciError(err,__FILE__,__LINE__)

typedef struct serverParams serverParams;
typedef struct clientParams clientParams;
typedef struct transferData transferData;

typedef enum  { INIT,DATA,END} transferType;

/*This is allways sendt, cast or memcpy first part of the data segment to get this.
For data: totalSize gives the size of the whole data transfer, sizeof this struct included.
Individual size varibels gives the info that is needed to calculate where in the data segment
the different datas are lockated.
*/
struct transferData {
  transferType type; //type of segmen, Init, Data, End
  unsigned int totalSize; //total size of data in the segment
  unsigned int width; //image parameter
  unsigned int height;//image parameter
  unsigned int frameNo; 

  // The cut, measured in rows of macroblocks
  unsigned int topRow;
  unsigned int nRows;

  //size of image, send from master to slave
  unsigned int size_Y;
  unsigned int size_U;
  unsigned int size_V;
  //size of recons, is sendt from slave to  master and from master to slave
  unsigned int r_size_Y;
  unsigned int r_size_U;
  unsigned int r_size_V;

  //size of MV, sendt from slave to master
  unsigned int mv_size_Y;
  unsigned int mv_size_U;
  unsigned int mv_size_V;

  //size of Q, sendt from slave to master
  unsigned int q_size_Y;
  unsigned int q_size_U;
  unsigned int q_size_V;

};


struct serverParams {
  unsigned int          localNodeId;
  unsigned int          localSegmentId;
  unsigned int          remoteNodeId;
  unsigned int          interruptNo;
  unsigned int          offset;
  unsigned int          segmentSize; 
  volatile uint8_t      *localMapAddr; //nvcc liker ikke denne, linje 89
  sci_desc_t            sd; 
  sci_local_segment_t   localSegment;
  sci_map_t             localMap;
  sci_local_interrupt_t localInterrupt;
};

struct clientParams {
  unsigned int           localNodeId;
  unsigned int           lockalSegmentId;
  unsigned int           remoteNodeId;
  unsigned int           remoteSegmentId;
  unsigned int           interruptNo;
  unsigned int           segmentSize;
  volatile uint8_t       *remoteMapAddr; //deprecated by MemCpy nvcc, liker ikke denne linje 173
  sci_desc_t             sd;
  sci_remote_segment_t   remoteSegment;
  sci_map_t              localMap;
  sci_map_t              remoteMap;
  sci_remote_interrupt_t remoteInterrupt;
};


sci_error_t sisciInit(unsigned int lAdN,unsigned int *localNodeId);
sci_error_t makeServerSegment(serverParams *server);
sci_error_t removeServer(serverParams *server);
sci_error_t makeClientSegment(clientParams *client);
sci_error_t removeClient(clientParams *client);
sci_error_t sendData(uint8_t *data,unsigned int size,clientParams *client);
sci_error_t reciveData(volatile uint8_t **data,unsigned int fjern,serverParams *server);
sci_error_t sisciExit();

void sisciError(sci_error_t error,const char *errFile,int lineNr);


/*
todo:
move interrupt back out from send and recive
*/
/*
changes:
3/11:
uses SciMemCpy
4/11:
segmantsize from size_t to unsigned int as dolfin uses that, and size_t is unsigned long.
changed type of send and recive data from uint til uint8_t
5/11:
Use uint8_t for send and recive
*/

#endif
